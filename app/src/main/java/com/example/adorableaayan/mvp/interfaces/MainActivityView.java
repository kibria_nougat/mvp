package com.example.adorableaayan.mvp.interfaces;

import android.content.Context;

import java.util.HashMap;

/**
 * Created by AdorableAayan on 10/13/2017.
 */

public interface MainActivityView {

    void showIpInfo(HashMap hashMap);
    void startLoading();
    void stopLoading();
    void showMessage(String message);
    Context getAppContext();
}
