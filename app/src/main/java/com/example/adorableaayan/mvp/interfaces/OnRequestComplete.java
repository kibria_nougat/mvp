package com.example.adorableaayan.mvp.interfaces;

import java.util.Objects;

/**
 * Created by AdorableAayan on 10/13/2017.
 */

public interface OnRequestComplete {
    void onRequestComplete(Object obj);
    void onRequestError(String errorMsg);
}
