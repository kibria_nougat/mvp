package com.example.adorableaayan.mvp.model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adorableaayan.mvp.Constantvalues;
import com.example.adorableaayan.mvp.interfaces.OnRequestComplete;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by AdorableAayan on 10/13/2017.
 */

public class InvokeApi {

    OnRequestComplete onRequestComplete;

    public InvokeApi(final Context context, final OnRequestComplete onRequestComplete) {

        this.onRequestComplete = onRequestComplete;
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constantvalues.URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("OnResponse", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            HashMap hashMap = new HashMap();
                            hashMap.put("ip", jsonObject.getString("ip"));
                            hashMap.put("country", jsonObject.getString("country"));
                            hashMap.put("location", jsonObject.getString("loc"));
                            onRequestComplete.onRequestComplete(hashMap);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                onRequestComplete.onRequestError("Something went wrong.");
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
