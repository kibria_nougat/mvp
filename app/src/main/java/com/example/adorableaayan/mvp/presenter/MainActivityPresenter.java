package com.example.adorableaayan.mvp.presenter;

import com.example.adorableaayan.mvp.interfaces.MainActivityView;
import com.example.adorableaayan.mvp.interfaces.OnRequestComplete;
import com.example.adorableaayan.mvp.model.InvokeApi;

import java.util.HashMap;

/**
 * Created by AdorableAayan on 10/13/2017.
 */

public class MainActivityPresenter {

    MainActivityView mainActivityView;
    public MainActivityPresenter(MainActivityView mainActivityView){
        this.mainActivityView = mainActivityView;
    }

    public void getIpInformation(){
        mainActivityView.startLoading();

        new InvokeApi(mainActivityView.getAppContext(), new OnRequestComplete() {
            @Override
            public void onRequestComplete(Object obj) {
                mainActivityView.stopLoading();
                mainActivityView.showIpInfo((HashMap) obj);
            }

            @Override
            public void onRequestError(String errorMsg) {
                mainActivityView.showMessage(errorMsg);
            }
        });
    }
}
