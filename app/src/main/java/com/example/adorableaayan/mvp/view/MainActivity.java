package com.example.adorableaayan.mvp.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.adorableaayan.mvp.R;
import com.example.adorableaayan.mvp.interfaces.MainActivityView;
import com.example.adorableaayan.mvp.presenter.MainActivityPresenter;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements MainActivityView{

    Button info_Button;
    TextView ipInfo_TextView;
    ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ipInfo_TextView = (TextView) findViewById(R.id.ipInfo_TextView);
        info_Button = (Button) findViewById(R.id.info_Button);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);

        final MainActivityPresenter mainActivityPresenter = new MainActivityPresenter(this);
        info_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivityPresenter.getIpInformation();
            }
        });
    }

    @Override
    public void showIpInfo(HashMap infoData) {


        String ipInfo = "IP: " + infoData.get("ip").toString()+"\n"+"Country: " + infoData.get("country").toString()+"\n"+
                        "Location: " + infoData.get("location").toString();

        ipInfo_TextView.setText(ipInfo);
    }

    @Override
    public void startLoading() {
        progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }
}
